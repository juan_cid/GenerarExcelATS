﻿using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Azure;
using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Files.Shares;
using Azure.Storage.Sas;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Management.Storage.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using NLog.Internal;
using PruebaAzureStorage.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using Syncfusion.XlsIO;

namespace PruebaAzureStorage
{
    class Program
    {
        public static readonly ProspectosRepository prospectosRepository = new ProspectosRepository();
        static async Task Main(string[] args)
        {

            var productos = prospectosRepository.GetAllxFecha(String.Format("{0:yyyyMMdd}", DateTime.Now));

            int contador = 0;
           


                ShareClient share = new ShareClient(ConfigurationManager.AppSettings["StorageConnectionString"], "exceldocument");
              
                ShareDirectoryClient directory = share.GetDirectoryClient("Excel");
            string nombrearchivo = "Prospectos_" + String.Format("{0:yyyyMMddHHmmss}", DateTime.Now) + ".xlsx";
                ShareFileClient file = directory.GetFileClient(nombrearchivo);

                var workbook = new XLWorkbook();

            IXLWorksheet worksheet = workbook.Worksheets.Add("Prospectos");
            worksheet.Cell(1, 1).Value = "Numero";
            worksheet.Cell(1, 2).Value = "nombre";
            worksheet.Cell(1, 3).Value = "RUT";
            worksheet.Cell(1, 4).Value = "Email";
            worksheet.Cell(1, 5).Value = "Sueldo";
            worksheet.Cell(1, 6).Value = "Afp Origen";
            worksheet.Cell(1, 7).Value = "Cod Afp Origen";
            worksheet.Cell(1, 8).Value = "Fono Fijo";
            worksheet.Cell(1, 9).Value = "Fono Movil";
            worksheet.Cell(1, 10).Value = "Fecha Ingreso";
            worksheet.Cell(1, 11).Value = "Canal";
            worksheet.Cell(1, 11).Value = "Horario";
            contador = 2;
            foreach (Prospectos pros in productos)
            {
                worksheet.Cell(contador, 1).Value = contador -1;
                worksheet.Cell(contador, 2).Value = pros.Nombre;
                worksheet.Cell(contador, 3).Value = pros.Rut;
                worksheet.Cell(contador, 4).Value = pros.Email;
                worksheet.Cell(contador, 5).Value = pros.Sueldo;
                worksheet.Cell(contador, 6).Value = pros.Afp_Origen;
                worksheet.Cell(contador, 7).Value = pros.Cod_Afp_Origen;
                worksheet.Cell(contador, 8).Value = pros.Fono_Fijo;
                worksheet.Cell(contador, 9).Value = pros.Fono_Movil;
                worksheet.Cell(contador, 10).Value = pros.Fec_ing_reg;
                worksheet.Cell(contador, 11).Value = pros.Canal;
                worksheet.Cell(contador, 11).Value = pros.Horario;
                contador++;
                Console.WriteLine(contador.ToString() + " " + pros.Rut + " " + pros.Nombre);
            }
            using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    using (FileStream stream1 = new FileStream("salida1.xlsx", FileMode.Create, FileAccess.Write))
                    {
                        stream.WriteTo(stream1);
                        stream1.Close();
                    }


                    using (FileStream streamLectura = File.OpenRead(@"salida1.xlsx"))
                    {
                        file.Create(streamLectura.Length);

                        streamLectura.Position = 0;
                        file.UploadRange(
                            new HttpRange(0, streamLectura.Length), streamLectura);

                    }
                }



            //envio
            SendGridClient sendGrid = new SendGridClient("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");
            SendGridMessage mensaje = new SendGridMessage();
            mensaje.PlainTextContent = "";
            var from = new EmailAddress("noreply@aumentatusueldo.cl", "noreply@aumentatusueldo.cl");
            var subject = "Aumenta tu Sueldo Carga de Simulaciones Diarias en repositorio";
            var to = new EmailAddress("nelson.diaz.n@gmail.com", "Nelson Diaz Navarro");
            var plainTextContent = "";
            var htmlContent = "Estimado usuario \n El archivo de simulaciones de la calculadora aumenta tu sueldo correspondiente a " + String.Format("{0:dd-MM-yyyy}", DateTime.Now) + " se encuentra en el repositorio con el nombre " + nombrearchivo +".";

            var msg = MailHelper.CreateSingleEmail(from, to, subject, htmlContent, "");
            var response = sendGrid.SendEmailAsync(msg).Result;

        }
    }
}
      
    

