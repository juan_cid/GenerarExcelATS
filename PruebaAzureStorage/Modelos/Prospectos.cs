﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaAzureStorage.Models
{
    public class Prospectos
    {

        public string Nombre { get; set; }
        public string Rut { get; set; }
        public string Email { get; set; }
        public float Sueldo { get; set; }
        public string Afp_Origen { get; set; }
        public string Cod_Afp_Origen { get; set; }

        public string Fono_Fijo { get; set; }
        public string Fono_Movil { get; set; }

        public string Fec_ing_reg { get; set; }
        public string Canal { get; set; }

        public string Horario { get; set; }
    }
}
